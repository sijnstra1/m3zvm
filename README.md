New Release 8. Published 09-Feb-2025 - check Changelog

New Release 7. Published 07-Jan-2024 - check Changelog

New Release 6. Published 16-Jul-2023 - check Changelog

Also available: [M1ZVM](https://gitlab.com/sijnstra1/m1zvm/) for Model I and [M4ZVM](https://gitlab.com/sijnstra1/m4zvm/) for Model 4/4P.  

If you would like downloads of pre-built Model III bootable disk images, or if you would like to support the development, please head to the [M4ZVM itch.io page](https://sijnstra.itch.io/m4zvm)

For a CP/M 3 or CP/M 2.2 version, use [Vezza](https://gitlab.com/sijnstra1/vezza/)

# Technical summary:
~~~
M3ZVM is a TRS-80 Model 3 48K Infocom/Inform/Z-machine game
    virtual machine. It's an adapted port of M4ZVM.
It can run v1-v5 inform format interactive fiction game files including
    Infocom and post-Infocom era story game files.
    *see features and limitations below
    Interactive Fiction games are available at numerous locations including:
    https://www.ifarchive.org/ just look for files with extension such as 
    .z1, .z2, .z3, .z4, .z5
The binaries in the zout directory should run on any DOS
**NOTE for REAL HARDWARE**
         While this software is intended to and does run on real hardware,
it is possible that some newer post-Infocom games, using a real floppy disk drive
where the game does not load entirely in to memory, are written in a way that causes
the drive to seek quite a lot. If you are concerned, try it on an emulator first or
use your judgement. Hard drives including FreHD are recommended.

Written by Shawn Sijnstra (c) 2021-2 under GPLv2
Feedback, comments and bug reports welcome - email me <firstname>@<surname>.com
About me: home page of my various projects and other work http://sijnstra.com
~~~
# Features:
~~~
Supports the core features needed including
* z1, z2, z3, z4, z5, z6, z7 and z8 games can execute
* Supports full 64x16 display (no trimmed last column unlike the original)
* Named game save and load
* Timed input
* Transcript (printing)
* Simulated reverse text highlighting by replacing spaces with blocks (intelligent sizing)
* Accented chatacters
* Fast execution of games (z3 games now execute faster than the original)
* z3 status line supports am/pm time including Cut Throats easter egg
* Supports Z-code 'Dynmem' dynamic memory size up to 22k
* 5KB Least-recently-used disk cache to minimise disk I/O
* 2KB z-machine stack size (over 50 call levels deep)
* arrows recongnised by game (some prompts now need shift-clear or shift-left to delete
  text such as Bureaucracy - this happens for character based input fields)
* Shift-Enter maps to ^ to support Bureaucracy
* Ctrl-A, Ctrl-F, Ctrl-G, Ctrl-H to edit via cursor left, cursor right, delete, backspace
  shift-left and shift-right also move the cursor left and right during line input
* command-line switch to optionally enable the infamous "Tandy bit". This will tweak the
  messages and the copyright block of some z3 games.
For release by release update technical details, check out the Changelog.
~~~
# Screenshots:
~~~
Sherlock (z5 game) running on trs80gp:
~~~
![Sherlock screenshot](M3-Sherlock-trs80-3.gif "Sherlock (z5 game) running on trs80gp")
~~~
The Hitchhiker's Guide to the Galaxy  (z5 game - with Invisiclues) running on trs80gp:
~~~
![Hitchhikers screenshot](HitchHikers_Invisiclues_trs80-3.gif "Hitchhiker's Guide (z5 game) running on trs80gp")
# Limitations and known bugs:
~~~
* Large and complex .z8 & .z5 games can execute slowly, especially if they use the inform7
  compiler or use the interpreter in a way that exceeds the compute power of 8 bits.
  The easiest rough guide is that post-infocom games are much more likely to run well if
  compiled using Inform6, especially the PunyInform library. Inform7 games are likely to
  be too complex.
* WARNING and Special note for *Inform 7* compiled games: The Inform 7 library is
  designed to use the stack a lot more than either Inform 6 or any of the genuine Infocom
  games. This means that even if an Inform 7 compiled game loads, and you are patient enough
  to wait for your next turn, as the game develops it may reach a point where the game runs
  out of stack and you can no longer progress that story. This would be disappointing to
  say the least..
* Other highlighting techniques not supported (italic, bold)
* Only one font (font 2 and font 3 not supported)
* Insufficient screen size to support Seastalker split screen
* Save/load error messages are minimal.
* To save space the debug feature has been removed.
* Games that require greater than 22K Dynamic Memory (e.g. Trinity) will not execute.
* Accented characters supported within hardware limitations, a default M3 mapping is
  included. Remaps characters outside of the font to make them displayable.
* No support for Undo (note that M3ZVM correctly reports this)
* No Colour or greyscale other than mapping to plain or reverse text where possible
* No support for sound (not even bleep)
* Transcript is to printer only, assumes same width as screen, and doesn't convert accented characters
* Upper window does not auto-expand, however, it does allow the text to be displayed regardless.
~~~

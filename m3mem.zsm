;	 M3ZVM Z-Code interpreter for the TRS-80 Model 3 running LDOS
;		Copyright (C) 2021-4 Shawn Sijnstra <shawn@sijnstra.com>
;		Requires minimum 128K, supports available @BANK driver memory
;		Caching routines and 64K memory model copyright Shawn Sijnstra
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;
; TRS-80 version note: I have moved the stack MUCH LOWER as I can only page
; in 32K from 8000H-FFFFH. This means the stack can remain in place even
; after bank switches.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;This will check to see how much will fit.
;
willitfit:
IFDEF	Model4
; 
; don't need to calculate how many banks, just whether at least 2 are available. So test banks, then (allhere)
; can store the max # for in-memory pages to compare with E in EHL
; @BANK
; Registers Affected: AF, BC, [HL if a transfer is requested].
	ld	b,18	;maximum should be only 16 for a 512K game, don't need to test so much
	ld	c,0
willitlp1:
	inc	c	;if we inc here, we won't increment in the wrong place in the loop
;	push	bc
;	ld	b,1	;release bank - this causes issues with the HyperBNK driver, but works for XR8er
;	svc	@BANK
;	pop	bc
	push bc
	ld	b,2	;test bank
	svc	@BANK
	pop	bc
;Bank Availability Test
;Entry:
;B 2; Test if bank C in use.
;C Bank number.
;Exit:
;NZ In use/not available.
	jr	nz,willi1
	djnz	willitlp1	; count how many available banks


willi1:		;how much is in c?
	dec	c	;last successful bank.
	ld	a,c
	srl	a	;halve it  each bank is 32k so how many 64k blocks do we have?
	or	a
	jr	nz,willit2
	ld	hl,nobanks	;NZ C = less than 64K banked memory available.
;	scf	;
;	ccf
	or	a	;reset carry
	ret

willit2:	;
	scf			;Z C = at least minimum banks are available.
	ret
ELSE
;	ld	a,(HIGH$+1)
;	cp	0fdh
	ld	hl,(high$)
	inc	hl	;round up for xxFFH as this will fit an entire sector
	ld	a,h
	dec	a	;take care of the edge case for high$=0ffffH
	cp	0f8h-1	;off by one. Allow up to 8 pages for high mem drivers (generous due to FreHD)
	jr	nc,willit2
	dec	hl
	ld	de,nohival		;show value of HIGH$
	call	sphex4
	ld	hl,nohimem
;	scf	;
;	ccf
	or	a	;reset carry
	ret
willit2:
;note that the himem (after increment) range of $f8..$00
;corresponds to -8..0, so we can add it
	ld		a,h		;restore A as MSB of HIGH$+1
	ld		hl,cachesize_fix
	add		a,(hl)
	ld		(hl),a		;lower available cache based on himem
;	ld		hl,zmp_up0
;	add		a,(hl)
;	ld		(hl),a		;load as many sectors as fit into memory - not any more
	scf
	ret
ENDIF
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
IFDEF	model4
lbank:	defb	0	;Current bank to load - check this starts at 1!
ENDIF
banks:	defb	0	;how many banks do we have/2 (i.e. how many 64K blocks above 64K)
io_zver:	defb	0	;Z-machine version
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
ldbank:
ifdef	model4
	push	bc
	ld	a,(lbank)
	add	a,30h
	ld	c,a
	SVC	@DSP		;display the bank number
	ld	a,(lbank)
	ld	c,a
	ld	b,0	;switch banks
	di
	SVC	@BANK
	ld	b,128		;128 records / bank... (as we are now 256 byte records, remains the same)
	ld	de,08000h	;Address within block

ldblp:	;push	bc
	push	de
	ld	de,FCB1	;story file
	SVC	@READ	;z if successful
	pop	de
	;pop	bc
	jr	z,ldblp2
	cp	28		;end of file encountered error - i.e. end of sequential read. Because of my laziness, this only happens if last record is 255 bytes.
	
	jr	z,lretz	;we may have a page edge condition here - need to check in 28 is returned if you try to read the non-existant
				;especially for partial sectors.
	cp	29		;Record number out of range - I'm being lazy and reading the last sector beyond the end of the last record (i.e. last recor <255 bytes)
	jr	z,lretz	;same as previous jr comment
	jr	lberr	;Error
ldblp2:		;continue

;;	push	bc	;critical that B is preserved.
	ld	a,b		;critical that B is preserved.
;	ld	a,(lbank)
;	ld	c,a
;	ld	b,0	;switch banks
;	SVC	@BANK
	ld	hl,F_BUF1
	ld	bc,256
	ldir		;Copy 100h bytes into high memory
;	SVC	@BANK	;for glory! Don't switch back till the end.
;;	pop	bc
	ld	b,a		;restore counter

	djnz	ldblp
	ld	bc,0	;I don't think b and c will be zero - this will do for now
	SVC	@BANK	;to fill in from above.
	ei
	pop	bc
;	ld	a,(lbank)
;	inc	a
;	ld	(lbank),a
	ld	hl,lbank
	inc	(hl)
retnz:	xor	a	;NZ C = not eof
	inc	a
	scf
	ret
;
lretz:	
	ld	bc,0	;I don't think b and c will be zero - this will do for now
	SVC	@BANK	;to fill in from above.
	ei
	pop	bc
	xor	a	;Z C = eof
	scf
	ret
;
lberr:	
	ld	bc,0	;I don't think b and c will be zero - this will do for now
	SVC	@BANK	;to fill in from above.
	ei
	pop	bc
	jp	xlterr
else
	push	bc
	ld	b,ZmemPages	;Lets try only loading game max- otherwise preloading too much
	;+cache_entries		;game maximum + cache to start
zmp_up0	equ	$-1		;in case of restart, will only reload Dynmem pages
	ld	de,Zmem0	;start of ZMEM

ldblp:	;push	bc
	push	de
	ld	de,FCB1	;story file
	SVC	@READ	;z if successful
	pop	de
	;pop	bc
	jr	z,ldblp2
	cp	28		;end of file encountered error - i.e. end of sequential read. Because of my laziness, this only happens if last record is 255 bytes.
	
	jr	z,lretz	;we may have a page edge condition here - need to check in 28 is returned if you try to read the non-existant
				;especially for partial sectors.
	cp	29		;Record number out of range - I'm being lazy and reading the last sector beyond the end of the last record (i.e. last recor <255 bytes)
	jr	z,lretz	;same as previous jr comment
	jr	lberr	;Error
ldblp2:		;continue
	ld	a,b		;critical that B is preserved.
	ld	hl,F_BUF1
	ld	bc,256
	ldir		;Copy 100h bytes into high memory
	ld	b,a		;restore counter
	djnz	ldblp
	pop	bc
retnz:	xor	a	;NZ C = not eof
	inc	a
	scf
	ret
;
lretz:
	pop	bc
	xor	a	;Z C = eof
	scf
	ret
;
lberr:	
	pop	bc
	jp	xlterr

ENDIF
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
ZXPEEKi:
peek1i:	
	ld	a,e
	or	a
	jr	nz,peek1cache
	ld	a,h
	cp	ZMemPages
zmp_up1	equ	$-1
	jp	c,peek64ai		;it's in memory otherwise in cache

peek1cache:
;	ld	a,(allhere)
;	cp	e				;Check to see if page is in memory
;	jp	nc,peekh64		;out of range now for jr
;
;Get byte at EHL from the disc file.
;
;Convert 24-bit byte address in EHL to record number in the FCB.
;

	push	de
	push	bc
	push	hl
; At this point in the code, EHL still has address. Need to see if EH matches any pages

;Least Recently Used cache
	ld	b,e
peek1cache_64:
	ld	c,h		;BC has EH From EHL. L recovered later.

	ld	a,(cachehead)
;need to add in the trivial case - what if the first is a match, just need to read without repointing.

;Rewriting so d = cachenext, e = cacheprev
;	ld	(cachenext),a
	ld	d,a
;	ld	e,a
	add	a,a
	add	a,d
;	ld	e,a			;4
;	ld	d,0			;7
	ld	hl,cachetop	;10
;	add	hl,de		;11 add 3x block number as we have 3 bytes/record.

	add	a,l			;4 - revert to this method as DE now used
	ld	l,a			;4
	jr	nc,cache_bskip	;12/7
	inc	h			;4

;15+4+7 (4 bytes) = 22 vs 20 (5 bytes) - except below is fast as it assumes
;d=0 the entire time. Saves 7 so becomes 19 vs 20.
cache_bskip:
	ld	a,(hl)
	cp	c
	inc	hl
;	jr	nz,cache_bnotyet
	jr	nz,cache_bskip1
	ld	a,(hl)
	cp	b
	jp	z,cache_hitbytetriv
;	jp	cache_bnotyet
cache_bskip1:
;relies on cache having at least 2 entries so doesn't need to test for end yet.
	inc	hl				
	ld	a,(hl)
cachelp_byte0:
	ld	e,d
	ld	d,a
	add	a,a				;limits to 85 entries max in the cache, saves 10 t-states per loop
	add	a,d
cachelp_byte:
	ld	hl,cachetop
	add	a,l			;4 Revert to this method as DE now used for next,prev
	ld	l,a			;4
	jr	nc,cache_bskip2	;12/7
	inc	h			;4
cache_bskip2:	
;	add	hl,de		;add 3x block number as we have 3 bytes/record.
;	add	hl,de		;11
;	add	hl,de

	ld	a,(hl)
	cp	c
	inc	hl
	jp	nz,cache_bnotyet	;silghtly favour no match instead of match
	ld	a,(hl)
	cp	b
	jr	z,cache_hitbyte
cache_bnotyet:
	inc	hl
	ld	a,(hl)
	cp	cache_entries		;last entry - so replace.
ce_up1	equ	$-1
;;	jr	z,cache_missbyte		; (end of the list)
	jp	nz,cachelp_byte0
;;	ld	e,d
;	ld	e,a				;4 moved to be earlier
;	ld	a,(cachenext)
;	ld	(cacheprev),a	;
;	ld	a,(hl)			;7 this should save 3 per loop
;	ld	a,e				;4
;	ld	(cachenext),a
;;	ld	d,a
;	ld	l,a				;+4
;;	add	a,a				;limits to 85 entries max in the cache, saves 10 t-states per loop
;;	add	a,d
;	ld	e,a
;	ld	d,0				;7 as a<256, d is always 0 for this loop
;;	jr cachelp_byte

;
;Record number is wrong. Load the correct one.
;
cache_missbyte:

;ldrec:
;	ld	b,e
;	ld	c,d	;bc=eh (from original EHL triad, which is now EDA')
;so now we are at the last record

;make the entry in cacheprev now point to record cachesize
;make head point to current

	ld	a,(cachehead)	;demote the old head
	ld	(hl),a
	dec	hl
	ld	(hl),b			;update record content to new block
	dec	hl				;backwards because it's more efficient
	ld	(hl),c

;	ld	a,(cachenext)	;New head of the list
	ld	a,d
	ld	(cachehead),a

;make the previous record now point to end.

;	ld	a,(cacheprev)
;	ld	e,a
	ld	a,e
	add	a,a
	add	a,e
;	ld	e,a
;	ld	d,0			;still 0 from all the previous calcs.
	ld	hl,cachetop+2	;start of the table + 2 to hit pointer and skip (EH)L
	add	a,l
	ld	l,a
	jr	nc,cache_missb2
	inc	h
cache_missb2:
;	inc	hl
;	inc	hl
	ld	(hl),cache_entries
ce_up2	equ	$-1
;load record into block pointed by head.
;BC Should already be EH
	ld	de,fcb1
	SVC	@POSN	;position in file (BC)
	SVC	@READ	;read into FCB1 file buffer
; Now copy the read buffer into the cache.
;need to work out the address again now of the destination.
; This should be cachestart + 100*block #. cachestart/256

	ld	a,(cachehead)
	ld	hl,F_BUF1
	ld	bc,256
	add	a,cachestart/256
cs_up1	equ	$-1
	ld	d,a
	ld	e,c	;should be 0 above.
	LDIR
;g	ld	e,0
;g	ld	(fcb1+3),de
;g	ld	de,fcb1
;g	ex	af,af'
;g	SVC	@READ	;read into FCB1 file buffer
;For LDOS and probably others I think you can change the buffer pointer in the FCB.
;  Quoting the Programmer's Guide to TRSDOS Version 6:
;6.4.4 Disk File Buffer Pointer - <Bytes 3-4>
;This is a pointer to the disk file buffer that is used for all disk I/O associated with the file.
; The pointer is a 16-bit address stored in normal low-order - high-order format. This pointer is
; the buffer address specified in register pair HL at open time.
;g	ex	af,af'
	jp		cache_readbyte
cache_hitbyte:
;Rewriting so d = cachenext, e = cacheprev

	inc		hl
	ld		c,(hl)	;prev now needs to point to this - this would be the following record after cachenext.
;	ld		c,a		;saves A. C restored at EXX beow
	ld		a,(cachehead)		;point to the old head
	ld		(hl),a
;	ld		a,(cachenext)
	ld		a,d
	ld		(cachehead),a
;	ld		a,(cacheprev)
;	ld		e,a
	ld		a,e
	add		a,a
	add		a,e
;	ld		e,a		;4
;;	ld		a,c		;don't retrieve. use C below. retrieve pointer to after cachenext
;	ld		d,0		;7 still 0
	ld		hl,cachetop+2	;start of the table + 2 to hit pointer and skip (EH)L
	add		a,l		;4
	ld		l,a		;4
	jr		nc,cache_hitb2	;12
	inc		h
cache_hitb2:
;	add		hl,de		;times 3, zero based.
;	add		hl,de
;	add		hl,de
;	inc		hl
;	inc		hl		;skip (EH)L 
	ld		(hl),c				;previous now linked to successor; i.e. other one cut out.


;Use the new cache head
cache_hitbytetriv: ;don't need to change any pointers - it's at the head.
;	ld	a,(cachehead)
	ld	a,d				;d has (cachehead) in both use cases
	add	a,cachestart/256
cs_up2	equ	$-1
cache_readbyte:
	pop	hl
	ld	c,h
	ld	h,a
	ld	a,(hl)
	ld	h,c
	pop	bc
	pop	de
;	pop	hl
;	scf	;testing
;ZXPEEK now increments each time
	inc	l
	ret	nz
	inc	h
	ret	nz
	inc	e
	ret	

;
ZXPK64i:	;fetch byte from first 64K and increment
;Preserves HL, BC, DE. Returns A.
;HL = upper memory location. Port settings assume inverse characters are active.
	ld		a,h
	cp		ZMemPages
zmp_up2	equ	$-1
	jr		c,peek64ai
	push	de
	push	bc
	push	hl
	ld		b,0
	jp		peek1cache_64
;	ld		e,0		;7 bytes
;	call	peek1cache	;
;	pop		de
;	ret

peek64ai:
;	ld		a,h		;preserve H
	ex		af,af'
	ld		a,Zmem0/256	;only need to adjust the MSB
	add		a,h
	ld		h,a
	ld		a,(hl)
	ex		af,af'	;+
	ld		h,a		;+
	ex		af,af'	;+
;ZXPEEK now increments each time
	inc	l
	ret	nz
	inc	h
	ret	nz
	inc	e
	ret	

ZXPK64rw:			;read/write memory only with no increment
peek64rw:			;read/write memory only
	ld		a,h		;preserve H
	ex		af,af'
	ld		a,Zmem0/256	;only need to adjust the MSB
	add		a,h
	ld		h,a
	ld		a,(hl)
	ex		af,af'	;+
	ld		h,a		;+
	ex		af,af'	;+
	ret




; Peek 16-bit word from memory.
; IMPORTANT: Note also this block of code has NO SAFETY for crossing page boundary
; 			do you checks for L=0FFH first.
; checks have been moved.
;peekw64:
;first check to see if loaded in memory
;	ld	a,(allhere)
;	cp	e
;	jp	nc,Peekw64_allmem		;out of range for jr now

;
;Get word at EHL from the disc file.
;
;Convert 24-bit byte address in EHL to record number in the FCB.
; Assumes boundary check already done!
;
peekw64_cachemem:

	push	de
	push	hl

; At this point in the code, EHL still has address. Need to see if EH matches any pages

	ld	b,e
	ld	c,h		;BCL has EHL

	ld	a,(cachehead)		;so we can multiply in 8 bits. Limits cache to 85 entries
;Rewriting so d = cachenext, e = cacheprev
;	ld	(cachenext),a
	ld	d,a
;	ld	e,a
	add	a,a
	add	a,d
;	ld	e,a			;4
;	ld	d,0			;7
	ld	hl,cachetop	;10
;	add	hl,de		;11 add 3x block number as we have 3 bytes/record.
	add	a,l			;4 - revert to this method as DE now used
	ld	l,a			;4
	jr	nc,cache_wskip	;12/7
	inc	h			;4
cache_wskip:
	ld	a,(hl)
	cp	c
	inc	hl
;	jr	nz,cache_wnotyet
	jr	nz,cache_wskip1
	ld	a,(hl)
	cp	b
	jp	z,cache_hitwordtriv
;	jp	cache_wnotyet
cache_wskip1:
;relies on cache having at least 2 entries so doesn't need to test for end yet.
	inc	hl				
	ld	a,(hl)
cachelp_word0:
	ld	e,d
	ld	d,a
	add	a,a				;limits to 85 entries max in the cache, saves 10 t-states per loop
	add	a,d
cachelp_word:
	ld	hl,cachetop
	add	a,l			;4 Revert to this method as DE now used for next,prev
	ld	l,a			;4
	jr	nc,cache_wskip2	;12/7
	inc	h			;4
cache_wskip2:	
	ld	a,(hl)
	cp	c
	inc	hl
	jp	nz,cache_wnotyet
	ld	a,(hl)
	cp	b
	jr	z,cache_hitword
cache_wnotyet:
	inc	hl
	ld	a,(hl)
	cp	cache_entries		;last entry - so replace.
ce_up3	equ	$-1
;;	jr	z,cache_missword		; (end of the list)
	jp	nz,cachelp_word0
;;	ld	e,d
;	ld	e,a				;4 moved to be earlier
;	ld	a,(cachenext)
;	ld	(cacheprev),a	;
;	ld	a,(hl)			;7 this should save 3 per loop
;	ld	a,e				;4
;	ld	(cachenext),a
;;	ld	d,a
;	ld	l,a				;+4
;;	add	a,a				;limits to 85 entries max in the cache, saves 10 t-states per loop
;;	add	a,d
;	ld	e,a
;	ld	d,0				;7 as a<256, d is always 0 for this loop
;;	jr cachelp_word
cache_missword:
;so now we are at the last record

;make the entry in cacheprev now point to record cachesize
;make head point to current

	ld	a,(cachehead)	;demote the old head
	ld	(hl),a
	dec	hl
	ld	(hl),b			;update record content to new block
	dec	hl				;backwards because it's more efficient
	ld	(hl),c

;	ld	a,(cachenext)	;New head of the list
	ld	a,d
	ld	(cachehead),a

;make the previous record now point to end.

;	ld	a,(cacheprev)
;	ld	e,a
	ld	a,e
	add	a,a
	add	a,e
;	ld	e,a
;	ld	d,0			;still 0 from all the previous calcs.
	ld	hl,cachetop+2	;start of the table + 2 to hit pointer and skip (EH)L
	add	a,l
	ld	l,a
	jr	nc,cache_missw2
	inc	h
cache_missw2:
;	add	hl,de
;	add	hl,de
;	add	hl,de
;	inc	hl
;	inc	hl
	ld	(hl),cache_entries	;update pointer to be end
ce_up4	equ	$-1

;load record into block pointed by head.
;BC Should already be EH
	ld	de,fcb1
	SVC	@POSN	;position in file (BC)
	SVC	@READ	;read into FCB1 file buffer
; Now copy the read buffer into the cache.
;need to work out the address again now of the destination.
; This should be cachestart + 100*block #. cachestart/256

	ld	a,(cachehead)
	ld	hl,F_BUF1
	ld	bc,256
	add	a,cachestart/256
cs_up3	equ	$-1
	ld	d,a
	ld	e,c	;should be 0 above.
	LDIR
;g	ld	e,0
;g	ld	(fcb1+3),de
;g	ld	de,fcb1
;g	ex	af,af'
;g	SVC	@READ	;read into FCB1 file buffer
;!!!!!!! Don't forget to patch verify if you try this again
;For LDOS and probably others I think you can change the buffer pointer in the FCB.
;  Quoting the Programmer's Guide to TRSDOS Version 6:
;6.4.4 Disk File Buffer Pointer - <Bytes 3-4>
;This is a pointer to the disk file buffer that is used for all disk I/O associated with the file.
; The pointer is a 16-bit address stored in normal low-order - high-order format. This pointer is
; the buffer address specified in register pair HL at open time.
;g	ex	af,af'
	jp		cache_readword	;a is already head + 58H

cache_hitword:
	inc		hl
	ld		c,(hl)	;prev now needs to point to this - this would be the following record after cachenext.
;	ld		c,a		;saves A. C restored at EXX beow
	ld		a,(cachehead)		;point to the old head
	ld		(hl),a
;	ld		a,(cachenext)
	ld		a,d
	ld		(cachehead),a
;	ld		a,(cacheprev)
;	ld		e,a
	ld		a,e
	add		a,a
	add		a,e
;	ld		e,a
;;	ld		a,c		;don't retrieve. use C below. retrieve pointer to after cachenext
;	ld		d,0		;still 0
	ld		hl,cachetop+2	;start of the table + 2 to hit pointer and skip (EH)L
	add		a,l
	ld		l,a
	jr		nc,cache_hitw2
	inc		h
cache_hitw2:
;	add		hl,de		;times 3, zero based.
;	add		hl,de
;	add		hl,de
;	inc		hl
;	inc		hl		;skip (EH)L 
	ld		(hl),c				;previous now linked to successor; i.e. other one cut out.


;Use the new cache head
cache_hitwordtriv:		;don't update any pointers if atthe top
;	ld	a,(cachehead)
	ld	a,d				;d has (cachehead) in both use cases
	add	a,cachestart/256			;cache memory starts at cachestart
cs_up4	equ	$-1
cache_readword:
	pop	hl
	ld	d,h
;	ld	c,h	
	ld	h,a		;relative location in a disk cache page.
;	ld	a,c
	
	ld	b,(hl)	;BC = word required
;	inc	hl
	inc	l		;page-aligned so h always remains
	ld	c,(hl)

;	dec	hl		;restore HL
	dec	l		;page-aligned so h always remains
	ld	h,d		;by unwinding above.
;	ld	h,a
	pop	de
;	pop	hl
;	scf	;testing
	ret

;
;Peek word, no auto-increment : return BC from address (EHL)
; improved test for edge condition so all in here now.
ZXPKWD:
peekw:
;pp	push	af
	ex	af,af'
	ld	a,e
	or	a
	jr	nz,peekw_cacheonly
	ld	a,h
;	cp	ZMemPages
	sub	ZMemPages
zmp_up3	equ	$-1
	jr	c,peekw_memchk
;	jp	c,Peekw64_low
;	jr	z,peekw_memchk
;	ld	a,(allhere)
;	cp	e
;	jp	nc,Peekw_memchk		;in mem check

peekw_cacheonly
	ld	a,l	;disk cache boundary check
	inc a	;should be a tiny bit faster detecting boundary
	jr	z,slowpkw	;on a boundary 
	call	peekw64_cachemem ;we know it's in disk cache and ok
;pp	pop	af
	ex	af,af'
;	scf	;testing
	ret

peekw_memchk: ;- doube-check?
	inc	a
;;;	cp	ZMemPages
;;;zmp_up4	equ	$-1 - remove in BDOS
;;	jr	nz,peekw_memchkok
	jp	nz,Peekw64_low	;not the top page
	ld	a,l		;memory page - check boundary condition
	inc	a		;only edge is last byte before end of cache
;	jp	nz,peekw_memchkok	;jump is L isn't FFh
	jp	nz,Peekw64_low
; Non-incrementing slow peek word
slowpkw:
	ex	af,af'	;pp
	push	af
	push	hl
	push	de
	call	peek1i	;updated for new increment only version
	ld	b,a
	call	peek1i
	ld	c,a
	pop	de
	pop	hl
	pop	af
;	scf	;testing
	ret

; Peek W64
; assume boundaries already checked, and HL, HL+1 in the same 32k page.
; and that AF is the first item on stack followed by return address.
Peekw64_low:
;	ld		a,h		;4 preserve H
;	ex		af,af'	;4 need to preserve it to save H
;	push	hl
	ld		a,Zmem0/256	;7 only need to adjust the MSB
	add		a,h		;4
	ld		h,a		;4
	ld		b,(hl)
	inc		hl
	ld		c,(hl)
;	pop		hl
;	dec		hl		;6
;	ex		af,af'	;4 +
;	ld		h,a		;4 + => 22. push + pull = 21. Save 1 clock cycle.
;;	dec		hl		;6
	dec		l		;4 - h is rebuilt
	sub		a,Zmem0/256	;7 undo above
	ld		h,a		;4 		;17 rather than 21. 4 clock cycles!
;pp	pop		af
	ex		af,af'
;zi	scf
	ret
;
;Peek word, with auto-increment. Address = EHL, returned value = BC
;Preserves A
ZXPKWI:
ipeekw:
;pp	push	af
	ex	af,af'
	ld	a,e
	or	a
	jr	nz,ipeekw_cacheonly
	ld	a,h
	sub	ZMemPages
zmp_up5	equ	$-1
	jr	c,ipeekw_memchk
;;	jp	c,Peekw64_lowI
;	jr	z,ipeekw_memchk

;	ld	a,(allhere)
;	cp	e
;	jp	nc,Peekw_memchk		;in mem check

ipeekw_cacheonly
	ld	a,l	;disk cache boundary check
	inc a	;should be a tiny bit faster detecting boundary
	jr	z,slowpiw; on a boundary
	call	peekw64_cachemem		;BC := word from cachemem
	inc	hl	;we know this one isn't on a boundary so don't check to increment E.
ipkw2:
;pp	pop	af
	ex	af,af'
	inc	l	;this seem to fail on Hibernated 1
	ret	nz
	inc	h
	ret	nz
	inc	e

;;	inc	hl
;;	ld	a,h
;;	or	l
;;	jp	nz,ipkw3
;;	inc	e
ipkw3:;;	pop	af
;	scf	;testing
	ret

ipeekw_memchk:
	inc	a
;;;	cp	ZMemPages
;;;zmp_up6	equ	$-1
;;;	jr	c,ipeekw_memchkok
	jp		nz,Peekw64_lowI
;ipeekw_memchk:
	ld	a,l		;memory page - check boundary condition
	inc	a
;	jp	nz,ipeekw_memchkok	;jump if L isn't FFh
	jr	z,slowpiw
;	jp		nz,Peekw64_lowI
Peekw64_lowI:
;	push	hl
;	ld		a,h		;preserve H
;	ex		af,af'	;need to preserve it to save H
	ld		a,Zmem0/256	;7 only need to adjust the MSB
	add		a,h		;4
	ld		h,a		;4
	ld		b,(hl)
	inc		hl
	ld		c,(hl)
;	dec		hl
;	ex		af,af'	;+
;	ld		h,a		;+
	ld		a,h		;in case it has changed
	sub		a,Zmem0/256	;7 undo above
	ld		h,a		;4 => 15 vs 21
;	pop		hl
;	jp		ipeekw44	;increment EHL assuming first inc within boundary
;	inc	hl	;we know the first one is entirely within a page so don't check e.
	inc	hl
;	ld	a,h	;not possible in <64K model
;	or	l
;	jp	nz,ipkw6i
;	inc	e
;ipkw6i:
;pp	pop	af
	ex	af,af'
	ret
;
;Fetch & increment where we know the first incremenent is across a page boundary of some sort
; be that memory page or disk page.
; therefore the second fetch is NOT across a boundary.
slowpiw:
	ex	af,af'	;pp
	push	af
	call	peek1i	;increment only now available
	ld	b,a	
	call	peek1i
	ld	c,a
	pop	af
	ret
;



; Low-64k poke. Easy. E is always 0.
; poke EHL,a
; uses AF'. Preserves the rest.
ZXPOKE:
poke1:
;	Affects F
;	Preserves A,HL,DE,BC
;	assume only pokes in valid memory NEW ASSUMPTION
;	push	hl
	ex		af,af'
	ld		a,Zmem0/256	;only need to adjust the MSB
	add		a,h
	ld		h,a
	ex		af,af'
	ld		(hl),a
	ex		af,af'	;+
	sub		a,Zmem0/256
	ld		h,a		;+	
	ex		af,af'	;+
;	pop		hl
;zi	scf
	ret

;;
ZXPOKW:
pokew:	;borrowed striaght from peekw
;Assume well behaved!
;no boundaries - all in memory for any pokes!
;	push	hl		;11
	ex		af,af'
	ld		a,Zmem0/256	;7	only need to adjust the MSB
	add		a,h		;4
	ld		h,a		;4
;	ex		af,af'
	ld		(hl),b
	inc		hl
	ld		(hl),c
	dec		hl
	sub		Zmem0/256	;7	-> restore h
	ld		h,a			;4
;	ex		af,af'	;+
;	ld		h,a		;+
	ex		af,af'	;+
;	pop		hl		;10 -> 21  vs 11
;zi	scf
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
ZXIHDR:
ihdr1:
;
;The header is at 0 in bank 1. So page it in and access it directly.
ihdr1a:
;
;Since I want to use the stack, the header gets paged in at 4000h.
; M4 - need to manually switch in page 1, which will make everything start at 8000h. Can't make it 4000h.
;	di
;	ld	c,1
;	ld	b,0	;switch banks
;	SVC	@BANK
;	ld	a,(8000h)	;Z-machine version
	ld	a,(Zmem0)	;Z-machien version
	ld	(io_zver),a
	ld	de,io_zvbuf
	ld	l,a
	ld	h,0		;Create the "invalid version" error
	call	spdec3
	ld	hl,io_zvbad
	ld	a,(io_zver)
	or	a
	jr	z,io_ihdr8	;0 is a bad version
IFNDEF	V6routines
	cp	6
	jr	z,io_ihdr8	;v6 is not supported on M3
ENDIF
	cp	9
	jr	nc,io_ihdr8	;9 and up is a bad version
;
;Version is acceptable
;
;nb: the Z-machine is big-endian, but the z80 is little-endian. So 
;   the LSB of a word will be in H.
;
okver:	cp	4		;v3 flags or v4 flags?
	ld	hl,(Zmem0+1)	;Flags 1
	ld	a,l
	jr	nc,v4flag
	and     10001111b       ; Reset bits: 6 (default font variable width?)
                            ;             4 (status line not available?)
tandybit:
    or      00100000b       ; Set bits:   5 (upper window available?)
                            ; Leave bits unchanged:
                            ;             7 (unused)
                            ;             3 (The legendary "Tandy" bit) - modified by command line
                            ;             2 (always set)
                            ;             1 (status line type)
                            ;             0 (unused)
	jr	cflag

v4flag: 
    and     11010000b       ; Reset bits: 5 (sound effects available?)
                            ;             3 (emphasis available?)
                            ;             2 (bold available?)
                            ;             1 (picture display available?)
                            ;             0 (colours available?)
    or      10010000b       ; Set bits:   7 (input time-out available?)
                            ;             4 (fixed-width available?)
                            ; Leave bits unchanged:
                            ;             6 (unused)
cflag:	ld	l,a
	ld	(Zmem0+1h),hl
	ld	hl,(Zmem0+10h)	;Flags 2
	ld	a,h
    and     00000111b       ; Reset bits: 7 (use sound effects?)
                            ;             6 (use colours?)
                            ;             5 (use mouse?)
                            ;             4 (use UNDO?)
                            ;             3 (use pictures?)
                            ; Leave bits unchanged:
                            ;             2 (v6 RAM: must redraw status?)
                            ;             1 (RAM: force fixed-width)
                            ;             0 (RAM: turn on transcript)
	ld	h,a
	res	0,l		;"Menus" bit
	ld	(Zmem0+10h),hl
	ld	hl,scrset5
	ld	de,Zmem0+20h
	ld	bc,8
	ldir

; Fetch whether there is a keyboard mapping? V5+ only. Change reverse character for z1-3.
	ld	a,(io_zver)
	cp	4
	jr	nc,ihdr5
	ld	a,143
	ld	(v3style),a
	jr	ihdr7
ihdr5:
	cp	5
	jr	c,ihdr7
	ld	hl,(Zmem0+2EH)	;Address of terminating characters table (bytes) - probably only BeyondZork
	ld	(inpv5term),hl
	cp	6
	jr	nz,ihdr7
	ld	hl,0902h	;8.3.2: In Version 6 it should write colours 2 and 9 (black and white), either way round,
	ld	(Zmem0+2CH),hl	;into the default background and foreground colours in bytes $2c and $2d of the header.
ihdr7:
	scf

	defb	0Eh	;LD C, which will swallow the AND A
io_ihdr8:	and	a
ihdr9:
;;	push	af ;preserve flags - you can't preserve flags around a SP switch. Incompatible with the SP below.
;;	ld	bc,0	;switch banks back
;;	SVC	@BANK
;;	ei
;;	pop	af ; restore flags
;	scf
	ret
;
;Screen settings for a v4 AND v5 game
;
; << v1.11 make our 'pixels' one character in size, so as not to 
;    upset Beyond Zork
;
scrset5:
scrh:	defb	screen_h,screen_w,0,screen_w
scrhp:	defb	0, screen_h, 1,1	;24x80 chars
scrhld:	equ	screen_h		;fixed height. Try to compile in rather than loaded from memory
;
; >> v1.11
;
io_zvbad:	defb	'Unsupported Z-code version '
io_zvbuf:	defb	'000. Versions 1-8 are supported',0AEh
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This defines where the z-stack goes. I'm now changing to flexing the
; cache so this is now static.
ZXTMEM:
tmem1:
IFDEF 	Model4
;	ld	hl,0	;Top of memory fetch
;	ld	b,0
;	SVC	@HIGH$
	ld	hl,(HIGH$)
;	ld	l,0
;	dec	h		;Added because I think there's rounding errors here.
;	dec	h		;Allow 512 bytes for our stack - stack is elsewhere.
;	dec	h
;	dec	h		;and 256 bytes for interrupt code - covered by himem and no extra interrupt code.
ELSE
	ld	hl,ZMem0	;0A800H
ENDIF	
	ret		
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
ZXRNDI:
rndi1:	
;	ld	iy,(timeloc)
	ld	iy,timer$	;heartbeat. 
	ld	d,(iy)	;System time - only d is used
	ld	e,(iy-113)	;seconds
	ret
;TIMER$ (Address = X'4288')
;TIME$ ( Address = X'4217'-X'4219')
; ----------------------------------
; Contains the time-of-day
; TIME$+0 .... Contains the seconds
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; File verify
;Entered with DBC = length. Start checking at byte 40h.
;
ZXVRFY:
fvrfy:	ld	a,40h
	ld	(vptr),a
	ld	hl,0				;set checksum to zero
	push	de

;	ld	de,F_BUF1
;	ld	(fcb1+3),de
;For LDOS and probably others I think you can change the buffer pointer in the FCB.
;  Quoting the Programmer's Guide to TRSDOS Version 6:
;6.4.4 Disk File Buffer Pointer - <Bytes 3-4>
;This is a pointer to the disk file buffer that is used for all disk I/O associated with the file.
; The pointer is a 16-bit address stored in normal low-order - high-order format. This pointer is
; the buffer address specified in register pair HL at open time.

	ld		de,FCB1

	svc		@rew
	svc		@read   ;Load the initial record
	pop		de
	call	vckbyt
	scf
	ret
;
vckbyt:	ld	a,d
	or	b
	or	c
	ret	z
	push	de
	push	bc
	push	hl
	call	vrbyte	
	pop	hl
	ld	e,a
	ld	d,0
	add	hl,de
	pop	bc
	pop	de
	dec	bc
	ld	a,b
	and	c
	inc	a
	jr	nz,vckbyt
	dec	d
	jr	vckbyt
;
vrbyte:	ld	a,(vptr)
	or	a
	call	z,vldrec	;reached end of 256 byte sector so load next
	ld	e,a
	ld	d,0
	ld	hl,F_BUF1
	add	hl,de
	inc	a
	ld	(vptr),a
	ld	a,(hl)
	ret
;
vldrec:	
	push	af
	ld	de,fcb1
	svc	@read
	pop	af
	ret
;
vptr:	defb	0
cache_entries:	equ		40-16	;39-16	;37-16	;Note that high mem use comes off this - up to 8 records.
;cacheprev:	defb	0
;cachenext:	defb	0
;MOVED TO BE UNINITIALISED SPACE IN M3ZVM.ASM
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
IFDEF	Relocated
cachehead:	defb	0					;faster for additions even though only 1 byte used.
Cachetop:		defw	0,0,0,0,0,0,0,0		;alternating for compactness. Address + 3x next record is stored here.
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0 ; Notes 64 better on LRU than 40, as opposed to FIFO where 40 topped out
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0	; 64 is here - maximum allowed
				defb	0,0,0,0,0,0,0,0
				defw	0,0,0,0,0,0,0,0	; 72 is here. Some games benefit from this.
				defb	0,0,0,0,0,0,0,0
;				defw	0,0,0,0,0,0,0,0	; 80 is here. 72-> 80 seems to make no difference although have improved optimisations.
;				defb	0,0,0,0,0,0,0,0	; note 84 is max due to 8 bits in cache calculations.
;FCB1_Cachetab_end:	defw	0
ENDIF

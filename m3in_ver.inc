;    M3ZVM: Z-Code interpreter for the Z80 processor - expanded and rewritten
;    Copyright (C) 2021-4 Shawn Sijnstra <shawn@sijnstra.com>
;
;    Based on:
;    ZXZVM: Z-Code interpreter for the Z80 processor
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

; 2022-03-27 - Shawn Sijnstra - Release 1 for the Model 3
; 2022-04-08 - Shawn Sijnstra - Release 2
;	    display speed improvment, adjustment to cache to be
;	    variable and larger, made the status bar size intelligent,
;	    code optimised, game save rewrite & speedup, z6 reintroduced,
;		cosmetic change to more, bugfix for checking insufficient memory,
;		allowed more upper memory for harddisk/FreHD users, allowed
;       all DOS versions to load
; 2022-04-24 - shawn Sijnstra - Release 3
;		fixed keymapping for up-arrow, pre-scrolled screen for cosmetics,
;		bugfixes, increased available upper memory for caching, 
;		rewrote the input routine scroll management, overall speed
;		improvements, use DOS error messages
; 2022-05-01 - Shawn Sijnstra - Release 4
;		Speed improvements from Garry Lancaster (ZPC in alt registers)
; 		fixed zpoke speed, reworked branch/call/return code
; 		reorganised memory again adding 3 sectors of cache
;		rewrote cache trawl routine to use registers for tracking
;		fixed a bug when turning on "stream 0"
;		allowed more cache as dynamic, updated 2op,1op decode
; 2022-08-07 - Shawn Sijnstra - Release 5
;		Changes to parsing var/1op/2op and into dispatch
;		Tweaks to buffered text out to improve display speed
;		Tweaks to cursor queries including wrap check
;		Redo stack and buffers to increase cache by 3 sectors
;		Multiple changes to cache routines, encode for speed improvement
;		Bugfix: timer routine fixed and scaled for M1/M3 interrupt differences
;		Bugfix: issues with M1/3 on z3 games with no location
;		Tweaks to object, attribute and property routine speed
;		Additional cache now allows for Border Zone and Shogun on M1/3 (FreHD recommended!)
;		Change to core tracking of exceptions/error handling
;		Fixed printing address and print speed in flushbuf
;		Fixed printing to properly honour simply chainging transcript bit
;		Rewrites to flush_buf, stream 2 and stream 3 for speed improvement
;		Tweaks to z6 routine efficiency
; 2023-07-16 - Shawn Sijnstra - Release 6
;		Cache streamlining, core stream 3 & charout tweaks
;		Major rewrite to dispatch routine, update to debug tracking
; 2023-10-20 - Shawn Sijnstra - Release 6a
;		Tweaking messages and memory map
;		6b - speed tweaks to call1s (-2T), tweak to return (HL=0 only), v3jin
;		6c - reduce file size converting zeroes to uninitialised space,
;				reduce debug footprint, reduce space in alphabet, minor speed tweak on
;				objects. Tweaks to res_more, swnd
;				bugfix on edge case of save
;		6d - removed support for old saved game versions, increased cache by 2 blocks
;		TODO: maybe the minimum RAM requirement can drop lower?
; 2023-01-07 - Shawn Sijnstra - Release 7
; 7a changes
;		changes to stream handling to speed up vmzchar
;		remove double-checking z version (monolithic build)
; 2025-02-09 Release 8 changes
;		redo core so that all ZXPEEK calls auto-increment. 2% speed improvement except Arthur
;		added extra sector cache, redid @copy_table backwards & scan_byte to be slightly faster.
;		removed surplus byte in z6 move_window, tweaked zxscur (cursor move)
;		tweaks to init routine, bwdcpy speed
;		optimised initial startup code 
;		bytes saved in vm0ops status line
;		moved era_all to include zxcls and adapt for initialize
;		optimized fwdcopy, tokenize, inibuf (zvm side)
;		major rewrite find_word, next_token
;		added another sector (saved another block)
;		Cosmetic bug - handle auto wrapped line followed by immediate line feed
;		 - ignore solo linefeed in this case.
;		changes to inibuf to use burnable memory space.
;		rearranged memory map in core VM to reduce load footprint.
;		change to formula for cache entries.
;TODO: move io_zvbad?
;TODO: change the input from ZXpk64RW to increment
;Done: Try using printwrd with compressed strings for opening banner. - seems to be very inefficient

	defb	'M3ZVM Release 8 Serial 250209'
